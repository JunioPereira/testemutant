﻿const express = require('express');
const router = express.Router();
const request = require('request');

var _user=null;

request('https://jsonplaceholder.typicode.com/users', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        _user = JSON.parse(body);
    }
});

// routes
router.get('/AllWebSites', AllWebSites);     // AllWebSites
router.get('/NameEmailCompany', NameEmailCompany);     // NameEmailCompany
router.get('/Endereco', Endereco);     // Endereco

module.exports = router;

function AllWebSites(req, res, next) 
{
    var _wetsites = [];
    if(_user!=null)
    {
        for(var i = 0; i < _user.length; i++)
        {
            _wetsites.push(_user[i].website);
        }
    }

   return _wetsites ? res.json(_wetsites) : res.sendStatus(404).catch(err=>new(err));
}

function NameEmailCompany(req, res, next) 
{
    var _object = [];
    if(_user!=null)
    {
        for(var i = 0; i < _user.length; i++)
        {
            _object.push({Name: _user[i].name, Email: _user[i].email, Company: _user[i].company.name});
        }
    }

    // sort by name
    var _byName = _object.slice(0);
    _byName.sort(function(a,b) {
        var x = a.Name.toLowerCase();
        var y = b.Name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });

   return _byName ? res.json(_byName) : res.sendStatus(404).catch(err=>new(err));
}

function Endereco(req, res, next) 
{
    var _newArray = filterItems('suite');

   return _newArray ? res.json(_newArray) : res.sendStatus(404).catch(err=>new(err));
}

function filterItems(query) {
    var newArray = _user.filter(function (el) {
        return el.address.suite.toLowerCase().indexOf(query.toLowerCase()) > -1;
    });

    return newArray;
}